module.exports = function(wallaby) {
    return {
        testFramework: "jest",
        files:         ["src/**/*.js"],
        tests:         ["tests/**/*.test.js"],

        env: {
            type:   "node",
        },

        compilers: {
            "**/*.js": wallaby.compilers.babel(),
        },
    };
};