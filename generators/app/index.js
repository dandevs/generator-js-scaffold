const Generator = require("yeoman-generator");
const fs        = require("fs");

module.exports = class extends Generator {
    prompting() {
        return this.prompt([
            {
                type:    "input",
                name:    "folderName",
                message: "Name of the new project:",
            },
            {
                type:     "confirm",
                name:     "eslint",
                message:  "Add .eslintrc.json to project",
            },
            {
                type:   "confirm",
                name:   "wallaby",
                message: "Add wallaby.conf.js to project",
            },
        ])
        .then((answers) => {
            this.props = answers;
        });
    }

    writing() {
        (() => { // Copy base files
            const filenames = fs.readdirSync(this.templatePath("base"));

            filenames.forEach((filename) => {
                this.fs.copyTpl(
                    this.templatePath("base/" + filename),
                    this.destinationPath(this.props.folderName + "/" + filename));
            });
        })();

        if (this.props.eslint) {
            this.fs.copyTpl(
                this.templatePath(".eslintrc.json"),
                this.destinationPath(this.props.folderName + "/.eslintrc.json"));
        }

        if (this.props.wallaby) {
            this.fs.copyTpl(
                this.templatePath("wallaby.conf.js"),
                this.destinationPath(this.props.folderName + "/wallaby.conf.js"));
        }
    }
}