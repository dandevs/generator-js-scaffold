const Generator = require("yeoman-generator");
const mkdirp = require("mkdirp")

module.exports = class extends Generator {
    prompting() {
        return this.prompt([{
            type:    "input",
            name:    "folder",
            message: "What is the new folder name:",
        }])
        .then((answers) => {
            this.props = answers;
        });
    }

    writing() {
        this.fs.copyTpl(
            this.templatePath("base"),
            this.destinationPath(this.props.folder));

        mkdirp.sync(this.destinationPath(this.props.folder + "/src"));
        mkdirp.sync(this.destinationPath(this.props.folder + "/tests"));
    }
}